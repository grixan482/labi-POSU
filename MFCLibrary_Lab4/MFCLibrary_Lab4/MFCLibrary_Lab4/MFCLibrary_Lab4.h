﻿// MFCLibrary_Lab4.h: основной файл заголовка для библиотеки DLL MFCLibrary_Lab4
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CMFCLibraryLab4App
// Реализацию этого класса см. в файле MFCLibrary_Lab4.cpp
//

class CMFCLibraryLab4App : public CWinApp
{
public:
	CMFCLibraryLab4App();

// Переопределение
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
