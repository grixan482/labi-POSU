#pragma once

#include <objbase.h>



// {C98924F8-A86F-4CF5-A5E1-B09554BCFF73}
static const IID IID_ICalc =
{ 0xc98924f8, 0xa86f, 0x4cf5, { 0xa5, 0xe1, 0xb0, 0x95, 0x54, 0xbc, 0xff, 0x73 } };

interface ICalc : public IUnknown
{
  // ���������������� ������� ������ ����������
  STDMETHOD_(float, Div) (float first, float second) = 0;
};

/*// {5B482171-BCBB-426E-BBE9-295B0D9FEE2A}
static const IID IID_ICalc =
{ 0x5b482171, 0xbcbb, 0x426e, { 0xbb, 0xe9, 0x29, 0x5b, 0xd, 0x9f, 0xee, 0x2a } };
*/