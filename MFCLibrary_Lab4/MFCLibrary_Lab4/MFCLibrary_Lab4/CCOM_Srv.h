﻿
#pragma once
#include "Interface.h"

// Целевой объект команды CCOM_Srv

class CCOM_Srv : public CCmdTarget
{
	//DECLARE_DYNAMIC(CCOM_Srv)
	DECLARE_DYNCREATE(CCOM_Srv)

public:
	CCOM_Srv();
	virtual ~CCOM_Srv();

protected:
	DECLARE_MESSAGE_MAP()

	DECLARE_OLECREATE(CCOM_Srv)
	BEGIN_INTERFACE_PART(Calc, ICalc)
	STDMETHOD_(float, Div)(float first, float second);
	END_INTERFACE_PART(Calc)
	
	DECLARE_DISPATCH_MAP() 
	DECLARE_INTERFACE_MAP()
};


