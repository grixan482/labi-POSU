﻿// CCOM_Srv.cpp: файл реализации
//

#include "pch.h"
#include "MFCLibrary_Lab4.h"
#include "CCOM_Srv.h"


// CCOM_Srv


/*// {73E176FE-5F45-4E90-829E-063F53C50673}
IMPLEMENT_OLECREATE(CCOM_Srv, "COM_Srv",
  0x73e176fe, 0x5f45, 0x4e90, 0x82, 0x9e, 0x6, 0x3f, 0x53, 0xc5, 0x6, 0x73);

*/
// {210B49F0-6C66-41EF-8E2E-13DB26E0440D}
IMPLEMENT_OLECREATE(CCOM_Srv, "COM_Srv",
	0x210b49f0, 0x6c66, 0x41ef, 0x8e, 0x2e, 0x13, 0xdb, 0x26, 0xe0, 0x44, 0xd);


//IMPLEMENT_DYNAMIC(CCOM_Srv, CCmdTarget)
IMPLEMENT_DYNCREATE(CCOM_Srv, CCmdTarget)

BEGIN_INTERFACE_MAP(CCOM_Srv, CCmdTarget)
  INTERFACE_PART(CCOM_Srv, CCOM_Srv::guid, Dispatch)
  INTERFACE_PART(CCOM_Srv, IID_ICalc, Calc)
END_INTERFACE_MAP()


BEGIN_DISPATCH_MAP(CCOM_Srv, CCmdTarget)
END_DISPATCH_MAP()


CCOM_Srv::CCOM_Srv()
{
}

CCOM_Srv::~CCOM_Srv()
{
}


BEGIN_MESSAGE_MAP(CCOM_Srv, CCmdTarget)
END_MESSAGE_MAP()



STDMETHODIMP_(ULONG) CCOM_Srv::XCalc::AddRef()
{
  TRACE("CCOM_Srv::XCalc::AddRef\n"); 
  METHOD_PROLOGUE(CCOM_Srv, Calc)
  return pThis->ExternalAddRef();
}

STDMETHODIMP_(ULONG) CCOM_Srv::XCalc::Release()
{
  TRACE("CYur::XCalc::Release\n"); 
  METHOD_PROLOGUE(CCOM_Srv, Calc)
  return pThis->ExternalRelease();
}

STDMETHODIMP CCOM_Srv::XCalc::QueryInterface(REFIID iid, LPVOID* ppvObj)
{
  TRACE("CYur::XCalc::QueryInterface\n"); 
  METHOD_PROLOGUE(CCOM_Srv, Calc)
  return pThis->ExternalQueryInterface(&iid, ppvObj);
}

STDMETHODIMP_(float) CCOM_Srv::XCalc::Div(float first, float second)
{
	float div;
	div = (first / second);
  return div;
}


// Обработчики сообщений CCOM_Srv
