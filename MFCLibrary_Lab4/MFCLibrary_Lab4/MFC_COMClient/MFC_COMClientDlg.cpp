﻿
// MFC_COMClientDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "MFC_COMClient.h"
#include "MFC_COMClientDlg.h"
#include "DlgProxy.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно CMFCCOMClientDlg


IMPLEMENT_DYNAMIC(CMFCCOMClientDlg, CDialogEx);

CMFCCOMClientDlg::CMFCCOMClientDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFC_COMCLIENT_DIALOG, pParent)
	, m_first(0)
	, m_second(0)
	, m_div(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pAutoProxy = nullptr;

	m_pICalc = NULL; 
	m_pIUnknown = NULL;
}

CMFCCOMClientDlg::~CMFCCOMClientDlg()
{
	// Если для данного диалогового окна используется прокси-сервер автоматизации, установите
	//  для его обратного указателя на это диалоговое окно значение NULL, чтобы он узнал об
	//  удалении диалогового окна.
	if (m_pAutoProxy != nullptr)
		m_pAutoProxy->m_pDialog = nullptr;
}

void CMFCCOMClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_first);
	DDX_Text(pDX, IDC_EDIT2, m_second);
	DDX_Text(pDX, IDC_EDIT3, m_div);
}

BEGIN_MESSAGE_MAP(CMFCCOMClientDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_CALC, &CMFCCOMClientDlg::OnBnClickedCalc)
END_MESSAGE_MAP()


// Обработчики сообщений CMFCCOMClientDlg

BOOL CMFCCOMClientDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	// TODO: добавьте дополнительную инициализацию

	HRESULT hResult; CLSID clsidSrv;
	/* Получаем идентификатор библиотеки */
	hResult = ::CLSIDFromProgID(L"COM_Srv", &clsidSrv); 
	if (FAILED(hResult))
	{
		/* Если возникла ошибка - выводим окно сообщения */
		::MessageBox(NULL, L"CLSIDFromProgID error", L"Error", MB_OK);
		return -1;
	}
	/* Загружаем библиотеку и получаем указатель на IUnknown */
	hResult = ::CoCreateInstance(clsidSrv, GetControllingUnknown(), CLSCTX_INPROC_SERVER, IID_IUnknown,
		(void**)&m_pIUnknown);
	if (FAILED(hResult))
	{
		/* Если возникла ошибка - обнуляем указатель на IUnknown и выводим окно сообщения */
		m_pIUnknown = NULL;
		::MessageBox(NULL, L"CoCreateInstance error", L"Error", MB_OK); 
		return -1;
	}
	/* получаем указатель на необходимый интерфейс */ 
	hResult = m_pIUnknown->QueryInterface(IID_ICalc, (void**) & m_pICalc); if (FAILED(hResult))
	{
		/* Если возникла ошибка - обнуляем указатель на интерфейс и выводим окно сообщения */
		m_pICalc = NULL;
		::MessageBox(NULL, L"QueryInterface error", L"Error", MB_OK); return -1;
	}


	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CMFCCOMClientDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CMFCCOMClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CMFCCOMClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// Серверы автоматизации не должны завершать работу при закрытии интерфейса пользователя,
//  если контроллеры по-прежнему удерживают один из его объектов.  Эти
//  обработчики сообщений гарантируют, что если прокси-сервер еще используется,
//  то пользовательский интерфейс скрывается, но диалоговое окно остается (если оно
//  закрыто).

void CMFCCOMClientDlg::OnClose()
{
	if (CanExit())
		CDialogEx::OnClose();
}

void CMFCCOMClientDlg::OnOK()
{
	if (CanExit())
		CDialogEx::OnOK();
}

void CMFCCOMClientDlg::OnCancel()
{
	if (CanExit())
		CDialogEx::OnCancel();
}

BOOL CMFCCOMClientDlg::CanExit()
{
	// Если объект прокси-сервера по-прежнему запущен, то контроллер
	//  автоматизации удерживает это приложение.  Оставьте
	//  диалоговое окно запущенным, но скройте его пользовательский интерфейс.
	if (m_pAutoProxy != nullptr)
	{
		ShowWindow(SW_HIDE);
		return FALSE;
	}

	return TRUE;
}



void CMFCCOMClientDlg::OnBnClickedCalc()
{
	// TODO: добавьте свой код обработчика уведомлений
	UpdateData();
	if (m_second == 0)
		::MessageBox(NULL, L" Нельзя делить на ноль!", L"Error", MB_OK);
	m_div = m_pICalc->Div(m_first, m_second); 
	UpdateData(FALSE);
}
