﻿
// MFC_COMClientDlg.h: файл заголовка
//

#pragma once


#include "..\MFCLibrary_Lab4\Interface.h"

class CMFCCOMClientDlgAutoProxy;


// Диалоговое окно CMFCCOMClientDlg
class CMFCCOMClientDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CMFCCOMClientDlg);
	friend class CMFCCOMClientDlgAutoProxy;

// Создание
public:
	CMFCCOMClientDlg(CWnd* pParent = nullptr);	// стандартный конструктор
	virtual ~CMFCCOMClientDlg();

	LPUNKNOWN m_pIUnknown; 
	ICalc* m_pICalc;


// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MFC_COMCLIENT_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	CMFCCOMClientDlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;

	BOOL CanExit();

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	virtual void OnOK();
	virtual void OnCancel();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedCalc();
	float m_first;
	float m_second;
	float m_div;
};
