﻿
// DlgProxy.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "MFC_COMClient.h"
#include "DlgProxy.h"
#include "MFC_COMClientDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCCOMClientDlgAutoProxy

IMPLEMENT_DYNCREATE(CMFCCOMClientDlgAutoProxy, CCmdTarget)

CMFCCOMClientDlgAutoProxy::CMFCCOMClientDlgAutoProxy()
{
	EnableAutomation();

	// Чтобы приложение работало, пока объект
	//	автоматизации активен, конструктор вызывает AfxOleLockApp.
	AfxOleLockApp();

	// Получает доступ к диалоговому окну через указатель
	//  главного окна приложения.  Задает внутренний указатель прокси-сервера для
	//  доступа к диалогу, а также задает обратный указатель диалога на этот
	//  прокси-сервер.
	ASSERT_VALID(AfxGetApp()->m_pMainWnd);
	if (AfxGetApp()->m_pMainWnd)
	{
		ASSERT_KINDOF(CMFCCOMClientDlg, AfxGetApp()->m_pMainWnd);
		if (AfxGetApp()->m_pMainWnd->IsKindOf(RUNTIME_CLASS(CMFCCOMClientDlg)))
		{
			m_pDialog = reinterpret_cast<CMFCCOMClientDlg*>(AfxGetApp()->m_pMainWnd);
			m_pDialog->m_pAutoProxy = this;
		}
	}
}

CMFCCOMClientDlgAutoProxy::~CMFCCOMClientDlgAutoProxy()
{
	// Чтобы прервать работу приложения, когда все объекты создаются
	// 	автоматически, деструктор вызывает AfxOleUnlockApp.
	//  Среди прочего будет уничтожено главное диалоговое окно
	if (m_pDialog != nullptr)
		m_pDialog->m_pAutoProxy = nullptr;
	AfxOleUnlockApp();
}

void CMFCCOMClientDlgAutoProxy::OnFinalRelease()
{
	// Когда будет освобождена последняя ссылка на объект автоматизации,
	// Будет вызван OnFinalRelease.  Базовый класс автоматически
	// удалит объект.  Перед вызовом базового класса требуется
	// дополнительная очистка объекта.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CMFCCOMClientDlgAutoProxy, CCmdTarget)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CMFCCOMClientDlgAutoProxy, CCmdTarget)
END_DISPATCH_MAP()

// Примечание. Мы добавили поддержку для IID_IMFC_COMClient, чтобы обеспечить безопасную с точки зрения типов привязку
//  из VBA.  Этот IID должен соответствовать GUID, связанному с
//  disp-интерфейс в файле .IDL.

// {55301a34-974c-4f0e-b9cb-476d8d9953f9}
static const IID IID_IMFC_COMClient =
{0x55301a34,0x974c,0x4f0e,{0xb9,0xcb,0x47,0x6d,0x8d,0x99,0x53,0xf9}};

BEGIN_INTERFACE_MAP(CMFCCOMClientDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CMFCCOMClientDlgAutoProxy, IID_IMFC_COMClient, Dispatch)
END_INTERFACE_MAP()

// Макрос IMPLEMENT_OLECREATE2 определен в pch.h этого проекта
// {e3713526-1d91-491c-9850-704ea526b9fb}
IMPLEMENT_OLECREATE2(CMFCCOMClientDlgAutoProxy, "MFC_COMClient.Application", 0xe3713526,0x1d91,0x491c,0x98,0x50,0x70,0x4e,0xa5,0x26,0xb9,0xfb)


// Обработчики сообщений CMFCCOMClientDlgAutoProxy
