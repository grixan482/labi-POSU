﻿
// MFC_COMClient.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CMFCCOMClientApp:
// Сведения о реализации этого класса: MFC_COMClient.cpp
//

class CMFCCOMClientApp : public CWinApp
{
public:
	CMFCCOMClientApp();

// Переопределение
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CMFCCOMClientApp theApp;
