﻿
// DlgProxy.h: файл заголовка
//

#pragma once

class CMFCCOMClientDlg;


// Целевой объект команды CMFCCOMClientDlgAutoProxy

class CMFCCOMClientDlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CMFCCOMClientDlgAutoProxy)

	CMFCCOMClientDlgAutoProxy();           // защищенный конструктор, используемый при динамическом создании

// Атрибуты
public:
	CMFCCOMClientDlg* m_pDialog;

// Операции
public:

// Переопределение
	public:
	virtual void OnFinalRelease();

// Реализация
protected:
	virtual ~CMFCCOMClientDlgAutoProxy();

	// Созданные функции схемы сообщений

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CMFCCOMClientDlgAutoProxy)

	// Автоматически созданные функции диспетчерской карты OLE

	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

