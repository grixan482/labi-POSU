﻿#pragma once

// MFCActiveXControl1Ctrl.h: объявление класса элемента управления ActiveX CMFCActiveXControl1Ctrl.


// CMFCActiveXControl1Ctrl: реализацию см. в MFCActiveXControl1Ctrl.cpp.

class CMFCActiveXControl1Ctrl : public COleControl
{
	DECLARE_DYNCREATE(CMFCActiveXControl1Ctrl)

// Конструктор
public:
	CMFCActiveXControl1Ctrl();

// Переопределение
public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();

// Реализация
protected:
	~CMFCActiveXControl1Ctrl();

	DECLARE_OLECREATE_EX(CMFCActiveXControl1Ctrl)    // Фабрика класса и guid
	DECLARE_OLETYPELIB(CMFCActiveXControl1Ctrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CMFCActiveXControl1Ctrl)     // ИД страницы свойств
	DECLARE_OLECTLTYPE(CMFCActiveXControl1Ctrl)		// Введите имя и промежуточное состояние

// Схемы сообщений
	DECLARE_MESSAGE_MAP()

// Схемы подготовки к отправке
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Схемы событий
	DECLARE_EVENT_MAP()

// Подготовка к отправке и ИД событий
public:
	enum {
		dispidshape = 2,
		dispidselect = 1
	};
	VARIANT_BOOL m_select;
	VARIANT_BOOL m_shape;
protected:
	VARIANT_BOOL Getselect();
	void Setselect(VARIANT_BOOL newVal);
	VARIANT_BOOL Getshape();
	void Setshape(VARIANT_BOOL newVal);
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
};

