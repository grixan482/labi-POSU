﻿// MFCActiveXControl1Ctrl.cpp: реализация класса элемента управления ActiveX CMFCActiveXControl1Ctrl.

#include "pch.h"
#include "framework.h"
#include "MFCActiveXControl1.h"
#include "MFCActiveXControl1Ctrl.h"
#include "MFCActiveXControl1PropPage.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CMFCActiveXControl1Ctrl, COleControl)

// Схема сообщений

BEGIN_MESSAGE_MAP(CMFCActiveXControl1Ctrl, COleControl)
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// Схема подготовки к отправке

BEGIN_DISPATCH_MAP(CMFCActiveXControl1Ctrl, COleControl)
	DISP_FUNCTION_ID(CMFCActiveXControl1Ctrl, "AboutBox", DISPID_ABOUTBOX, AboutBox, VT_EMPTY, VTS_NONE)
	DISP_PROPERTY_EX_ID(CMFCActiveXControl1Ctrl, "select", dispidselect, Getselect, Setselect, VT_BOOL)
	DISP_PROPERTY_EX_ID(CMFCActiveXControl1Ctrl, "shape", dispidshape, Getshape, Setshape, VT_BOOL)
END_DISPATCH_MAP()

// Схема событий

BEGIN_EVENT_MAP(CMFCActiveXControl1Ctrl, COleControl)
END_EVENT_MAP()

// Страницы свойств

// TODO: при необходимости добавьте дополнительные страницы свойств.  Не забудьте увеличить значение счетчика.
BEGIN_PROPPAGEIDS(CMFCActiveXControl1Ctrl, 1)
	PROPPAGEID(CMFCActiveXControl1PropPage::guid)
END_PROPPAGEIDS(CMFCActiveXControl1Ctrl)

// Инициализировать фабрику класса и guid

IMPLEMENT_OLECREATE_EX(CMFCActiveXControl1Ctrl, "MFCACTIVEXCONTRO.MFCActiveXControl1Ctrl.1",
	0x17485db8,0xf078,0x4401,0xa9,0x2e,0xa5,0xe0,0x62,0x2c,0x72,0x4e)

// Введите ИД и версию библиотеки

IMPLEMENT_OLETYPELIB(CMFCActiveXControl1Ctrl, _tlid, _wVerMajor, _wVerMinor)

// ИД интерфейса

const IID IID_DMFCActiveXControl1 = {0x3d4214cb,0xa509,0x41d0,{0xa5,0xda,0x90,0x15,0xd4,0xb1,0xe7,0x4a}};
const IID IID_DMFCActiveXControl1Events = {0xc2035108,0xe65d,0x4e1d,{0xba,0x8c,0xf7,0x11,0x8c,0x01,0xb2,0x0d}};

// Сведения о типах элементов управления

static const DWORD _dwMFCActiveXControl1OleMisc =
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CMFCActiveXControl1Ctrl, IDS_MFCACTIVEXCONTROL1, _dwMFCActiveXControl1OleMisc)

// CMFCActiveXControl1Ctrl::CMFCActiveXControl1CtrlFactory::UpdateRegistry -
// Добавляет или удаляет записи системного реестра для CMFCActiveXControl1Ctrl

BOOL CMFCActiveXControl1Ctrl::CMFCActiveXControl1CtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: убедитесь, что элементы управления следуют правилам модели изолированных потоков.
	// Дополнительные сведения см. в MFC TechNote 64.
	// Если элемент управления не соответствует правилам модели изоляции, то
	// необходимо модифицировать приведенный ниже код, изменив значение 6-го параметра с
	// afxRegApartmentThreading на 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_MFCACTIVEXCONTROL1,
			IDB_MFCACTIVEXCONTROL1,
			afxRegApartmentThreading,
			_dwMFCActiveXControl1OleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


// CMFCActiveXControl1Ctrl::CMFCActiveXControl1Ctrl — конструктор

CMFCActiveXControl1Ctrl::CMFCActiveXControl1Ctrl()
{
	InitializeIIDs(&IID_DMFCActiveXControl1, &IID_DMFCActiveXControl1Events);
	m_select = 1;
}

// CMFCActiveXControl1Ctrl::~CMFCActiveXControl1Ctrl — деструктор

CMFCActiveXControl1Ctrl::~CMFCActiveXControl1Ctrl()
{
	// TODO: Выполните здесь очистку данных экземпляра элемента управления.
}

// CMFCActiveXControl1Ctrl::OnDraw — функция рисования

void CMFCActiveXControl1Ctrl::OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	CPen pen;// объект типа «карандаш»
	CBrush fBrush, bBrush; //объекты типа «кисть» pdc ->SaveDC();
	pen.CreatePen(PS_SOLID, 1, RGB(0, 0, 0)); //цвет карандаша черный
	bBrush.CreateSolidBrush(TranslateColor(AmbientBackColor())); //задание цвета заднего фона
	fBrush.CreateSolidBrush(Getselect() ? RGB(255, 0, 0) : RGB(0, 255, 0)); //задание переднего фона
	pdc->FillRect(rcBounds, &bBrush);//задание заднего фона в виде закрашенного прямоугольника цветом заднего фона
	pdc->SelectObject(&pen);
	pdc->SelectObject(&fBrush); // выбор другой «кисти»
	if (m_shape) // задание формы объекты
		pdc->Ellipse(&rcBounds);//рисование эллипса
	else
		pdc->Rectangle(&rcBounds);//рисование прямоугольника
	pdc->RestoreDC(-1);
}

// CMFCActiveXControl1Ctrl::DoPropExchange — поддержка сохраняемости

void CMFCActiveXControl1Ctrl::DoPropExchange(CPropExchange* pPX)
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	// TODO: Вызывать функции PX_ для каждого постоянного настраиваемого свойства.
}


// CMFCActiveXControl1Ctrl::OnResetState — сброс элемента управления к состоянию по умолчанию

void CMFCActiveXControl1Ctrl::OnResetState()
{
	COleControl::OnResetState();  // Сбрасывает значения по умолчанию, найденные в DoPropExchange

	// TODO: Сбросьте здесь состояние любого другого элемента управления.
}


// CMFCActiveXControl1Ctrl::AboutBox — отображение окна "О программе" для пользователя

void CMFCActiveXControl1Ctrl::AboutBox()
{
	CDialogEx dlgAbout(IDD_ABOUTBOX_MFCACTIVEXCONTROL1);
	dlgAbout.DoModal();
}


// Обработчики сообщений CMFCActiveXControl1Ctrl


VARIANT_BOOL CMFCActiveXControl1Ctrl::Getselect()
{
	return m_select;
}


void CMFCActiveXControl1Ctrl::Setselect(VARIANT_BOOL newVal)
{
	m_select = newVal;
	InvalidateControl(); // стандартная функция MFC перерисовывающая объект.
}


VARIANT_BOOL CMFCActiveXControl1Ctrl::Getshape()
{
	return m_shape;
}


void CMFCActiveXControl1Ctrl::Setshape(VARIANT_BOOL newVal)
{
	m_shape = newVal;
	InvalidateControl(); // стандартная функция MFC перерисовывающая объект.
}


void CMFCActiveXControl1Ctrl::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: добавьте свой код обработчика сообщений или вызов стандартного
	m_select = !m_select;
	InvalidateControl();
}
