﻿// PolyCtl.h: объявление CPolyCtl
#pragma once
#include "resource.h"       // основные символы
#include <atlctl.h>
#include "Polygon_i.h"
#include <math.h>



#if defined(_WIN32_WCE) && !defined(_CE_DCOM) && !defined(_CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA)
#error "Однопотоковые COM-объекты не поддерживаются должным образом платформой Windows CE, например платформами Windows Mobile, в которых не предусмотрена полная поддержка DCOM. Определите _CE_ALLOW_SINGLE_THREADED_OBJECTS_IN_MTA, чтобы принудить ATL поддерживать создание однопотоковых COM-объектов и разрешить использование его реализаций однопотоковых COM-объектов. Для потоковой модели в вашем rgs-файле задано значение 'Free', поскольку это единственная потоковая модель, поддерживаемая не-DCOM платформами Windows CE."
#endif

using namespace ATL;

// CPolyCtl
class ATL_NO_VTABLE CPolyCtl :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CStockPropImpl<CPolyCtl, IPolyCtl>,
	public IOleControlImpl<CPolyCtl>,
	public IOleObjectImpl<CPolyCtl>,
	public IOleInPlaceActiveObjectImpl<CPolyCtl>,
	public IViewObjectExImpl<CPolyCtl>,
	public IOleInPlaceObjectWindowlessImpl<CPolyCtl>,
	public ISupportErrorInfo,
	public ISpecifyPropertyPagesImpl<CPolyCtl>,
	public IProvideClassInfo2Impl<&CLSID_PolyCtl, nullptr, &LIBID_PolygonLib>,
	public CComCoClass<CPolyCtl, &CLSID_PolyCtl>,
	public CComControl<CPolyCtl>
{
public:
	//CPolyCtl()
	//{
	//}

	DECLARE_OLEMISC_STATUS(OLEMISC_RECOMPOSEONRESIZE |
	OLEMISC_CANTLINKINSIDE |
		OLEMISC_INSIDEOUT |
		OLEMISC_ACTIVATEWHENVISIBLE |
		OLEMISC_SETCLIENTSITEFIRST
		)

		DECLARE_REGISTRY_RESOURCEID(IDR_POLYCTL)


	BEGIN_COM_MAP(CPolyCtl)
		COM_INTERFACE_ENTRY(IPolyCtl)
		COM_INTERFACE_ENTRY(IDispatch)
		COM_INTERFACE_ENTRY(IViewObjectEx)
		COM_INTERFACE_ENTRY(IViewObject2)
		COM_INTERFACE_ENTRY(IViewObject)
		COM_INTERFACE_ENTRY(IOleInPlaceObjectWindowless)
		COM_INTERFACE_ENTRY(IOleInPlaceObject)
		COM_INTERFACE_ENTRY2(IOleWindow, IOleInPlaceObjectWindowless)
		COM_INTERFACE_ENTRY(IOleInPlaceActiveObject)
		COM_INTERFACE_ENTRY(IOleControl)
		COM_INTERFACE_ENTRY(IOleObject)
		COM_INTERFACE_ENTRY(ISupportErrorInfo)
		COM_INTERFACE_ENTRY(ISpecifyPropertyPages)
		COM_INTERFACE_ENTRY(IProvideClassInfo)
		COM_INTERFACE_ENTRY(IProvideClassInfo2)
	END_COM_MAP()

	BEGIN_PROP_MAP(CPolyCtl)
		PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
		PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
#ifndef _WIN32_WCE
		PROP_ENTRY_TYPE("FillColor", DISPID_FILLCOLOR, CLSID_StockColorPage, VT_UI4)
#endif
		// Записи примеров
		// PROP_ENTRY_TYPE("Имя свойства", dispid, clsid, vtType)
		// PROP_PAGE(CLSID_StockColorPage)
	END_PROP_MAP()

	BEGIN_MSG_MAP(CPolyCtl)
		CHAIN_MSG_MAP(CComControl<CPolyCtl>)
		DEFAULT_REFLECTION_HANDLER()
	END_MSG_MAP()
	// Прототипы обработчика:
	//  LRESULT MessageHandler(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
	//  LRESULT CommandHandler(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
	//  LRESULT NotifyHandler(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

	// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid)
	{
		static const IID* const arr[] =
		{
			&IID_IPolyCtl,
		};

		for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
		{
			if (InlineIsEqualGUID(*arr[i], riid))
				return S_OK;
		}
		return S_FALSE;
	}

	// IViewObjectEx
	DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)

	// IPolyCtl
public:
	// IPolyCtl public:
	//STDMETHOD(get_Sides)(/*[out, retval]*/ short* newVal);
	//STDMETHOD(put_Sides)(/*[in]*/ short newVal);
	void CalcPoints(const RECT& rc);



	OLE_COLOR m_clrFillColor;
	short m_nSides;
	POINT m_arrPoint[100];
	void OnFillColorChanged()
	{
		ATLTRACE(_T("OnFillColorChanged\n"));
	}

	CPolyCtl()
	{
		m_nSides = 6;
		m_clrFillColor = RGB(0, 255, 0);
	}

	HRESULT CPolyCtl::OnDraw(ATL_DRAWINFO& di)
	{
		RECT& rc = *(RECT*)di.prcBounds; HDC hdc = di.hdcDraw;
		COLORREF colFore;
		HBRUSH hOldBrush, hBrush; HPEN hOldPen, hPen;
		// преобразуем m_colFore в тип COLORREF OleTranslateColor(m_clrFillColor, NULL, &colFore);
		OleTranslateColor(m_clrFillColor, NULL, &colFore);
		// Создаём и выбираем в контекст устройства перо и кисть
		hPen = (HPEN)GetStockObject(BLACK_PEN); hOldPen = (HPEN)SelectObject(hdc, hPen);
		hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH); hOldBrush = (HBRUSH)SelectObject(hdc, hBrush);
		Ellipse(hdc, rc.left, rc.top, rc.right, rc.bottom);
		// Создаём и выбираем в контекст устройства кисть, которой
		// будем заливать многоугольник hBrush = CreateSolidBrush(colFore); SelectObject(hdc,hBrush);
		hBrush = CreateSolidBrush(colFore); SelectObject(hdc, hBrush);
		CalcPoints(rc);
		Polygon(hdc, &m_arrPoint[0], m_nSides);
		// Возвращаем в контекст устройства ранее сохранённые
		// перо и кисть (hOldPen и hOldBrush) SelectObject(hdc, hOldPen); SelectObject(hdc, hOldBrush); DeleteObject(hBrush);
		SelectObject(hdc, hOldPen); SelectObject(hdc, hOldBrush); DeleteObject(hBrush);
		return S_OK;
	}

	STDMETHODIMP CPolyCtl::get_Sides(short* pVal)
	{
		*pVal = m_nSides; return S_OK;
	}
	STDMETHODIMP CPolyCtl::put_Sides(short newVal)
	{
		if (newVal > 2 && newVal < 101)
		{
			m_nSides = newVal;
			FireViewChange();
			return S_OK;
		}
		else
			return Error(_T("Число сторон должно быть между 3 и 100"));
	}


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}
};

OBJECT_ENTRY_AUTO(__uuidof(PolyCtl), CPolyCtl)
