﻿// dllmain.h: объявление класса модуля.

class CPolygonModule : public ATL::CAtlDllModuleT< CPolygonModule >
{
public :
	DECLARE_LIBID(LIBID_PolygonLib)
	DECLARE_REGISTRY_APPID_RESOURCEID(IDR_POLYGON, "{e96cbc5f-a8cf-4f4f-a81c-3ea62207786c}")
};

extern class CPolygonModule _AtlModule;
